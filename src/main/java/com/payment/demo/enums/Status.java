package com.payment.demo.enums;

public enum Status {

	NEW("NEW"), AUTHORIZED("AUTHORIZED"), CAPTURED("CAPTURED");

	Status(String status) {
		this.status = status;
	}

	public final String status;

	public static Status typeOf(String type) {
		Status[] values = Status.values();
		for (int i = 0; i < values.length; i++) {
			if (values[i].status.equals(type)) {
				return values[i];
			}
		}
		return null;
	}
}
