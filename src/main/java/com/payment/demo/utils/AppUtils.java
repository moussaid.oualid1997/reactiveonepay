package com.payment.demo.utils;

import static com.payment.demo.utils.AppConstants.MAX_PAGE_SIZE;

import org.springframework.http.HttpStatus;

import com.payment.demo.exception.DataException;

public class AppUtils {
	public static void validatePageNumberAndSize(int page, int size) throws DataException {
		if (page < 0) {
			throw new DataException(HttpStatus.BAD_REQUEST, "Page number cannot be less than zero.");
		}

		if (size < 0) {
			throw new DataException(HttpStatus.BAD_REQUEST, "Size number cannot be less than zero.");
		}

		if (size > MAX_PAGE_SIZE) {
			throw new DataException(HttpStatus.BAD_REQUEST, "Page size must not be greater than " + MAX_PAGE_SIZE);
		}
	}
}