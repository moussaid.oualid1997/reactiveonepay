package com.payment.demo.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.payment.demo.model.Transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionDto extends AbstractDto<Transaction> {

	private Long id;
	private String paymentType;
	private String status;
	private BigDecimal amount;

	public TransactionDto(Transaction model) {
		this.id = model.getId();
		this.paymentType = model.getPaymentType();
		this.status = model.getStatus();
		this.amount = model.getAmount();
	}
}
