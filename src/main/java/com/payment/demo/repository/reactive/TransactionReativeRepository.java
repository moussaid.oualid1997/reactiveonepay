package com.payment.demo.repository.reactive;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.payment.demo.model.Transaction;

@Repository
public interface TransactionReativeRepository extends ReactiveCrudRepository<Transaction, Long> {

}
