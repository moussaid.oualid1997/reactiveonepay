package com.payment.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.payment.demo.model.Transaction;
import com.payment.demo.service.reactive.TransactionReactiveService;

import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/transactionsStream")
public class TransactionReactiveController {

	@Autowired
	private TransactionReactiveService reactiveService;

	@GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public Flux<Transaction> getStream() {
		return reactiveService.getAllTransactions();
	}
}
