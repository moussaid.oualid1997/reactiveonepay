package com.payment.demo;

import java.math.BigDecimal;
import java.util.Random;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer;
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator;

import com.payment.demo.enums.PaymentTypes;
import com.payment.demo.model.Transaction;
import com.payment.demo.service.reactive.TransactionReactiveService;

import io.r2dbc.spi.ConnectionFactory;

@SpringBootApplication
public class PaymentReactiveDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentReactiveDemoApplication.class, args);
	}

	@Bean
	ConnectionFactoryInitializer initializer(ConnectionFactory connectionFactory) {

		ConnectionFactoryInitializer initializer = new ConnectionFactoryInitializer();
		initializer.setConnectionFactory(connectionFactory);
		initializer.setDatabasePopulator(new ResourceDatabasePopulator(new ClassPathResource("schema.sql")));

		return initializer;
	}

	@Bean
	CommandLineRunner start(TransactionReactiveService transactionReactiveService) {
		return args -> {
			Random random = new Random();
			Stream.of(PaymentTypes.CREDIT_CARD.type, PaymentTypes.PAYPAL.type, PaymentTypes.GIFT_CARD.type)
					.forEach(paymentType -> {
						while (true) {
							Transaction tr = new Transaction();
							tr.setPaymentType(paymentType);
							tr.setAmount(BigDecimal.valueOf(Math.abs(random.nextDouble())));
							transactionReactiveService.save(tr).block();

							try {
								Thread.sleep(500);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					});

		};
	}
}
