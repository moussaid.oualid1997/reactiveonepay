package com.payment.demo.service.reactive;

import com.payment.demo.model.Transaction;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TransactionReactiveService {

	public Mono<Transaction> save(Transaction t);

	public Flux<Transaction> getAllTransactions();
}
