package com.payment.demo.service.reactive.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.payment.demo.model.Transaction;
import com.payment.demo.repository.reactive.TransactionReativeRepository;
import com.payment.demo.service.reactive.TransactionReactiveService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TransactionReactiveServiceImpl implements TransactionReactiveService {

	@Autowired
	TransactionReativeRepository transactionRepository;

	@Override
	public Flux<Transaction> getAllTransactions() {
		return transactionRepository.findAll();
	}

	@Override
	public Mono<Transaction> save(Transaction t) {
		return transactionRepository.save(t);
	}

}
