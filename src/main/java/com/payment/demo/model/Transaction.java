package com.payment.demo.model;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;

import com.payment.demo.enums.Status;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Transaction extends AbstractModel {
	@Id
	private Long id;
	private String paymentType;
	private String status;
	private BigDecimal amount;

	public Transaction() {
		this.status = Status.NEW.status;
	}

	public void setStatus(String newStatus) {
		if (!Status.CAPTURED.status.equals(this.status)) {
			if (Status.CAPTURED.status.equals(newStatus)) {
				if (Status.AUTHORIZED.status.equals(this.status))
					this.status = newStatus;
			} else {
				this.status = newStatus;
			}
		}
	}
}
